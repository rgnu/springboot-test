package com.app.controller

import spock.lang.Specification

class HelloControllerSpec extends Specification {

  HelloController controller

  void setup() {
      controller = new HelloController()
  }

  void 'test hello() args=#args'() {

    given: 'a name'
      def name   = args.name

    when: 'call hello'
      Map response = controller.hello(name)

    then: 'response should be instance of Map'
      response instanceof Map

    and:
      response == expects.result

    where:
      [args, expects] << [
        // Case 1: Ok
        [
          [name: 'alphaar'],
          [result: [message: 'Hi alphaar !']]
        ]
      ]
  }
}
