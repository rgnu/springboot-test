package com.app.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

@RestController
class HelloController {

  @RequestMapping("/hello/{name}")
  Map hello(@PathVariable String name) {
    [ message: "Hi ${name} !" ];
  }
}
